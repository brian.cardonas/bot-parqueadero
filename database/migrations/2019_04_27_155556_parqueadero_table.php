<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParqueaderoTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('parqueadero', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slot')->unique()->comment('Número del parqueadero');
            $table->string('placa')->nullable()->unique()->comment('Placa del carro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('parqueadero');
    }
}
