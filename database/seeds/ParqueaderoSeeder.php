<?php

use Illuminate\Database\Seeder;
use App\Parqueadero;

class ParqueaderoSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for($i = 1; $i<=9; $i++){
            Parqueadero::create([
                'slot' => $i,
            ]);
        }
        Parqueadero::create([
            'slot' => 10,
            'placa' => 'ABC456'
        ]);
    }
}
