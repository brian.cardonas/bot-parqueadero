<?php
use App\Http\Controllers\BotManController;
use App\Parqueadero;

$botman = resolve('botman');

$botman->hears('/ayuda', function ($bot) {
    $bot->reply("Hola, puedes gestionar el parqueadero enviando alguno de los siguientes comandos: \n" .
    "parquear {placa} en {slot} - permite parquear el vehículo identificado con {placa} en la ubicación {slot} si ésta se encuentra disponible.  Ejemplo: parquear ABC123 en 3 \n" .
    "buscar {placa} - permite buscar un vehículo específico identificado con {placa} entre las ubicaciones del parqueadero.  Ejemplo: buscar ABC123 \n" .
    "retirar {placa} de {slot} - permite retirar el vehículo identificado con {placa} de la ubicación {slot} si éste se encuentra realmente parqueado allí.  Ejemplo: retirar ABC123 de 3.");
});

$botman->hears('parquear {placa} en {slot}', function ($bot, $placa, $slot) {
    $espacio = Parqueadero::buscar($slot);
    if($espacio) {
        if($espacio->estaDisponible()) {
            if(Parqueadero::buscarCarro($placa)) {
                $bot->reply("Lo siento :(, el carro $placa ya se encuentra parqueado. Debes retirarlo primero para continuar.");    
            } else {
                $espacio->placa = $placa;
                if($espacio->save()) {
                    $bot->reply("El carro identificado con placa $placa ha sido parqueado en $slot correctamente.");
                }
            }
        } else {
            $bot->reply("Lo siento :(, el slot $slot no está disponible en este momento.");
        }
    } else {
        $bot->reply("Lo siento :(, el slot $slot no existe. Actualmente tenemos 10 parqueaderos.");
    }
});

$botman->hears('buscar {placa}', function ($bot, $placa) {
    $carro = Parqueadero::buscarCarro($placa);
    if($carro) {
        $bot->reply("El carro identificado con placa $placa se encuentra parqueado en el slot $carro->slot.");
    } else {
        $bot->reply("El carro identificado con placa $placa no se encuentra en nuestro parqueadero.");
    }
});

$botman->hears('retirar {placa} de {slot}', function ($bot, $placa, $slot) {
    $espacio = Parqueadero::buscarCarro($placa, $slot);
    if($espacio) {
        $espacio->placa = null;
        if($espacio->save()) {
            $bot->reply("El carro identificado con placa $placa fue retirado correctamente del slot $slot.");
        }
    } else {
        $bot->reply("El carro identificado con placa $placa no se encuentra parqueado en el slot $slot.");
    }
});