<?php

namespace Tests\BotMan;

use Illuminate\Foundation\Inspiring;
use Tests\TestCase;

class AyudaTest extends TestCase {
    //Prueba que la respuesta de ayuda sea la indicada
    public function testMensajeAyuda() {
        $this->bot
            ->receives('/ayuda')
            ->assertReply("Hola, puedes gestionar el parqueadero enviando alguno de los siguientes comandos: \n" .
            "parquear {placa} en {slot} - permite parquear el vehículo identificado con {placa} en la ubicación {slot} si ésta se encuentra disponible.  Ejemplo: parquear ABC123 en 3 \n" .
            "buscar {placa} - permite buscar un vehículo específico identificado con {placa} entre las ubicaciones del parqueadero.  Ejemplo: buscar ABC123 \n" .
            "retirar {placa} de {slot} - permite retirar el vehículo identificado con {placa} de la ubicación {slot} si éste se encuentra realmente parqueado allí.  Ejemplo: retirar ABC123 de 3.");
    }
}
