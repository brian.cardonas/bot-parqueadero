<?php

namespace Tests\BotMan;

use Illuminate\Foundation\Inspiring;
use Tests\TestCase;

class ParqueaderoTest extends TestCase {
    //Prueba parquear un carro en un slot disponible
    public function testParquearEnSlotDisponible() {
        $this->bot
            ->receives('parquear ABC123 en 1')
            ->assertReply("El carro identificado con placa ABC123 ha sido parqueado en 1 correctamente.");
    }

    //Prueba parquear un carro en un slot no disponible
    public function testParquearEnSlotNoDisponible() {
        $this->bot
            ->receives('parquear ABC456 en 10')
            ->assertReply("Lo siento :(, el slot 10 no está disponible en este momento.");
    }

    //Prueba parquear un carro en un slot que no existe
    public function testParquearEnSlotNoExiste() {
        $this->bot
            ->receives('parquear ABC456 en 11')
            ->assertReply("Lo siento :(, el slot 11 no existe. Actualmente tenemos 10 parqueaderos.");
    }

    //Prueba parquear un carro que ya está parqueado
    public function testParquearUnCarroYaParqueado() {
        $this->bot
            ->receives('parquear ABC123 en 7')
            ->assertReply("Lo siento :(, el carro ABC123 ya se encuentra parqueado. Debes retirarlo primero para continuar.");
    }

    //Prueba buscar un carro por su placa
    public function testBuscarUnCarroYaParqueado() {
        $this->bot
            ->receives('buscar ABC123')
            ->assertReply("El carro identificado con placa ABC123 se encuentra parqueado en el slot 1.");
    }

    //Prueba buscar un carro por su placa que no está parqueado
    public function testBuscarUnCarroQueNoEstaParqueado() {
        $this->bot
            ->receives('buscar XET456')
            ->assertReply("El carro identificado con placa XET456 no se encuentra en nuestro parqueadero.");
    }

    //Prueba des-parquear un carro
    public function testRetirarUnCarroParqueadero() {
        $this->bot
            ->receives('retirar ABC123 de 1')
            ->assertReply("El carro identificado con placa ABC123 fue retirado correctamente del slot 1.");
    }

    //Prueba des-parquear un carro que no está parqueado
    public function testRetirarUnCarroNoParqueadero() {
        $this->bot
            ->receives('retirar YZW123 de 4')
            ->assertReply("El carro identificado con placa YZW123 no se encuentra parqueado en el slot 4.");
    }
}
