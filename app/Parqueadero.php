<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Representa el espacio donde se ubican carros de manera temporal.
 * 
 * Por ejemplo, el parqueadero de un Centro Comercial.
 */
class Parqueadero extends Model {
    protected $table = "parqueadero";

    protected $fillable = ['slot', 'placa'];

    /**
     * Busca la información del parqueadero por slot.
     * 
     * @param Integer $slot slot
     * @return Parqueadero $model el modelo
     */
    public static function buscar($slot) {
        return Parqueadero::where('slot', $slot)->first();
    }

    /**
     * Verifica si el parqueadero está disponible.
     * 
     * @return boolean true si está disponible, false de lo contrario
     */
    public function estaDisponible() {
        return ($this->placa) ? false : true;
    }

    /**
     * Busca la información de un carro en el parqueadero por su placa.
     * 
     * @param String $placa placa del carro
     * @param Integer $slot slot del parqueadero (opcional)
     * @return Parqueadero $model el modelo
     */
    public static function buscarCarro($placa, $slot = null) {
        if($slot) {
            return Parqueadero::where([
                'placa' => $placa,
                'slot' => $slot
            ])->first();
        }
        return Parqueadero::where('placa', $placa)->first();
    }
}
